﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.ScenesManger
{
    public class Select_LV : MonoBehaviour
    {
        [SerializeField] private Button lv1;
        [SerializeField] private Button lv2;
        [SerializeField] private Button back;
    
        private void Awake()
        {
            lv1.onClick.AddListener(Onlv1ButtonClicked);
            lv2.onClick.AddListener(Onlv2ButtonClicked);
            back.onClick.AddListener(OnBackButtonClicked);
        }
    
        private void Onlv1ButtonClicked()
        {
            SceneManager.LoadScene("Level01");
        }
    
        private void Onlv2ButtonClicked()
        {
            SceneManager.LoadScene("Level02");
        }
    
        private void OnBackButtonClicked()
        {
            SceneManager.LoadScene("Menu");
        }
    }
}
