﻿using UnityEngine;

namespace Assets.Scripts
{
    public class LoopBG : MonoBehaviour
    {
        public float speed = 4f;

        private Vector3 startPosition;
        
        // Start is called before the first frame update
        void Start()
        {
            startPosition = transform.position;
        }

        // Update is called once per frame
        void Update()
        {
            transform.Translate(Vector3.down * speed * Time.deltaTime);
            if (transform.position.y < -30f)
            {
                transform.position = startPosition;
            }
        }
    }
}
