﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Manager;
using Assets.Scripts.Spaceship;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Score01 : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private Button Restart;
    [SerializeField] private Button Back;
    [SerializeField] private bool Islevel1;
    private int Scoreplayer;
    void Start()
    {
        Scoreplayer = ScoreManager.Instance.PlayerScore;
        scoreText.text = $"All Score have {Scoreplayer} points";
        Restart.onClick.AddListener(OnReStartButtonClicked);
        Back.onClick.AddListener(OnBackButtonClicked);
    }

    private void OnBackButtonClicked()
    {
        SceneManager.LoadScene("Select LV");
    }

    private void OnReStartButtonClicked()
    {
        if (Islevel1 == true)
        {
            SceneManager.LoadScene("Level01");
        }
        else
        {
            SceneManager.LoadScene("Level02");
        }
    }
}
