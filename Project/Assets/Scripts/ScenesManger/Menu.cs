﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.ScenesManger
{
    public class Menu : MonoBehaviour
    {
        [SerializeField] private Button Start;
        [SerializeField] private Button quit;
    
        private void Awake()
        {
            Start.onClick.AddListener(OnStartButtonClicked);
            quit.onClick.AddListener(OnQuitButtonClicked);
        }
    
        private void OnStartButtonClicked()
        {
            SceneManager.LoadScene("Select LV");
        }
    
        private void OnQuitButtonClicked()
        {
            Application.Quit();
        }
    }
}
