﻿using System;

namespace Assets.Scripts.Spaceship
{
    public interface IDamagable 
    {
        event Action OnExploded;
        void TakeHit(int damage);
        void Explode();
    }
}

