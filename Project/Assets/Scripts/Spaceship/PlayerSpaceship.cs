﻿using System;
using Assets.Scripts.Manager;
using UnityEngine;

namespace Assets.Scripts.Spaceship
{
    public class PlayerSpaceship : BaseSpaceship, IDamagable
    {
        public event Action OnExploded;
        public Bullet Bomd;
        public static PlayerSpaceship Instance { get; private set; }
        private void Awake()
        {
            Debug.Assert(defaultBullet != null, "dafaultBullet cannot be null");
            Debug.Assert(gunPosition != null, "gunPosition cannot be null");
            audioSource = gameObject.GetComponent<AudioSource>();
            
            if (Instance == null)
            {
                Instance = this;
            }
        }

        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        
        public void FireBomd()
        {
            var bullet = Instantiate(Bomd, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);
            SoundManager.Instance.Play(SoundManager.Sound.PlayerFire);
        }

        public override void Fire()
        {
            var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
            bullet.Init(Vector2.up);
            SoundManager.Instance.Play(SoundManager.Sound.PlayerFire);
        }

        public void TakeHit(int damage)
        {
            Hp -= damage;
            if(Hp > 0)
            {
                return;
            }
            Explode();
        }

        public void Explode()
        {
            SoundManager.Instance.Play(SoundManager.Sound.PlayerExplode);
            Debug.Assert(Hp <= 0, "Hp is more than zero");
            Destroy(gameObject,5);
            OnExploded?.Invoke();
        }
    }

}


