﻿using System;
using Assets.Scripts.Manager;
using Unity.Mathematics;
using UnityEngine;


namespace Assets.Scripts.Spaceship
{
    public class EnemySpaceship : BaseSpaceship, IDamagable
    {
        public event Action OnExploded;
        
        [SerializeField] private float fireRate = 1;
        
        private float fireCounter = 0;
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }

        public override void Fire()
        {
            fireCounter += Time.deltaTime;
            if (fireCounter >= fireRate)
            {
                SoundManager.Instance.Play(SoundManager.Sound.EnemyFire);
                var bullet = Instantiate(defaultBullet, gunPosition.position, quaternion.identity);
                bullet.Init(Vector2.down);
                fireCounter = 0;
            }
        }
        public void TakeHit(int damage)
        {
            Hp -= damage;
            if(Hp > 0)
            {
                return;
            }

            Explode();
        }
        
        public void Explode()
        {
            SoundManager.Instance.Play(SoundManager.Sound.EnemyExplode);
            Debug.Assert(Hp <= 0, "Hp is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject,5);
            OnExploded?.Invoke();
        }
    }

}
