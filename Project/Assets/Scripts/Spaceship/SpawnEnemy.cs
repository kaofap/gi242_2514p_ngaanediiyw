﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Manager;
using Assets.Scripts.Spaceship;
using UnityEngine;

public class SpawnEnemy : MonoBehaviour
{
    private float nextSpawnTime;

    [SerializeField] private EnemySpaceship Enemy;
    [SerializeField] private int EnemyHp;
    [SerializeField] private int EnemySpeed;
    [SerializeField] private float spawnDelay = 0.5f;
    private AudioSource audioSource;
    private int PlayerScore = 1;
    internal EnemySpaceship spawnedEnemy;

    private void Awake()
    {
        //Spawn();
    }

    private void Update()
    {
        SpawnedEnemy();
    }

    public void SpawnedEnemy()
    {
        if (ShouldSpawn())
        {
            Spawn();
        } 
    }
       

    private void Spawn()
    {
        nextSpawnTime = Time.time + spawnDelay;
        spawnedEnemy = Instantiate(Enemy, transform.position, transform.rotation);
        spawnedEnemy.Init(EnemyHp,EnemySpeed);
        spawnedEnemy.OnExploded += OnEnemyDead;
            
    }

    private void OnEnemyDead()
    {
        SoundManager.Instance.Play(SoundManager.Sound.EnemyExplode);
        ScoreManager.Instance.SetScore(1);
    }

    private bool ShouldSpawn()
    {
        return Time.time >= nextSpawnTime;
    }
}
