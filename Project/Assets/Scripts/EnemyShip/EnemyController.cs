﻿using Assets.Scripts.Manager;
using Assets.Scripts.Spaceship;
using UnityEngine;

namespace Assets.Scripts.EnemyShip
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemySpaceship enemySpaceship;
        [SerializeField] private float chasingThresholdDistance;
        private PlayerSpaceship spawnedPlayerShip;

        private void Update()
        {
            MoveToPlayer();
            enemySpaceship.Fire();
        }

        private void MoveToPlayer()
        {
            spawnedPlayerShip = GameManager.Instance.spawnedPlayership;
            if (spawnedPlayerShip == null)
            {
                return;
            }
            var distanceToPlayer = Vector2.Distance(spawnedPlayerShip.transform.position, transform.position);

            if (distanceToPlayer > chasingThresholdDistance)
            {
                var direction = (spawnedPlayerShip.transform.position - transform.position);
                direction.Normalize();
                var distance = direction * enemySpaceship.Speed * Time.deltaTime;
                gameObject.transform.Translate(distance);
            }
            
        }
    }
    
}

