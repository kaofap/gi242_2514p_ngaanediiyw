﻿using Assets.Scripts.Manager;
using Assets.Scripts.Spaceship;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Assets.Scripts.PlayerShip
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] private float FireCoolDown = 0.3f;
        [SerializeField] private float BombCoolDown = 1;
        private float FireDelayReady;
        private bool Bomb01CoolDown = false;
        private bool fire01CoolDown = false;
        private Vector2 movementInput = Vector2.zero;
        private ShipInputActions inputActions;
        private float minX;
        private float maxX;
        private float minY;
        private float maxY;
        

        private void Awake()
        {
            InitInput();
            CreateMovementBoundary();
            
            if (GameManager.Instance.Fire01 == null)
            {
                return;
            }
            GameManager.Instance.Fire01.fillAmount = 0;
            GameManager.Instance.Bomb.fillAmount = 0;
        }
        
        private void Update()
        {
            Move();
            OnFire(default);
            OnBomb(default);
        }

        private void InitInput()
        {
            inputActions = new ShipInputActions();
            inputActions.Player.Move.performed += OnMove;
            inputActions.Player.Move.canceled += OnMove;
            inputActions.Player.Fire.performed += OnFire;
            inputActions.Player.Bomb.performed += OnBomb;
            inputActions.Player.Dead.performed += OnDead;
        }

        private void OnDead(InputAction.CallbackContext obj)
        {
            playerSpaceship.TakeHit(999999999);
        }

        public void OnBomb(InputAction.CallbackContext obj)
        {
            if (inputActions.Player.Bomb.triggered && Bomb01CoolDown  == false)
            {
                playerSpaceship.FireBomd();
                GameManager.Instance.Bomb.fillAmount = 1;
                Bomb01CoolDown  = true;
            }

            if (Bomb01CoolDown == true)
            {
                GameManager.Instance.Bomb.fillAmount -= 1 / BombCoolDown * Time.deltaTime;

                if (GameManager.Instance.Bomb.fillAmount <= 0)
                {
                    GameManager.Instance.Bomb.fillAmount = 0;
                    Bomb01CoolDown  = false;
                }
            }
        }

        private void OnFire(InputAction.CallbackContext obj)
        {
            if (inputActions.Player.Fire.triggered && fire01CoolDown == false)
            {
                playerSpaceship.Fire();
                GameManager.Instance.Fire01.fillAmount = 1;
                fire01CoolDown = true;
            }

            if (fire01CoolDown == true)
            {
                GameManager.Instance.Fire01.fillAmount -= 1 / FireCoolDown * Time.deltaTime;

                if (GameManager.Instance.Fire01.fillAmount <= 0)
                {
                    GameManager.Instance.Fire01.fillAmount = 0;
                    fire01CoolDown = false;
                }
            }
        }
        public void OnMove(InputAction.CallbackContext obj)
        {
            if(obj.performed)
            {
                movementInput = obj.ReadValue<Vector2>();
            }

            if(obj.canceled)
            {
                movementInput = Vector2.zero;
            }
        }

        private void Move()
        {
            var inputVelocity = movementInput * playerSpaceship.Speed;

            var newPosition = transform.position;

            newPosition.x = transform.position.x + inputVelocity.x * Time.smoothDeltaTime;
            newPosition.y = transform.position.y + inputVelocity.y * Time.smoothDeltaTime;

            newPosition.x = Mathf.Clamp(newPosition.x, minX, maxX);
            newPosition.y = Mathf.Clamp(newPosition.y, minY, maxY);

            transform.position = newPosition;


        }
        private void CreateMovementBoundary()
        {
            var mainCamera = Camera.main;
            Debug.Assert(mainCamera != null, "Main camera cannot be null");

            var spriteRenderer = playerSpaceship.GetComponent<SpriteRenderer>();

            var offset = spriteRenderer.bounds.size;
            minX = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).x + offset.x / 2;
            maxX = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).x - offset.x / 2;
            minY = mainCamera.ViewportToWorldPoint(mainCamera.rect.min).y + offset.y / 2;
            maxY = mainCamera.ViewportToWorldPoint(mainCamera.rect.max).y - offset.y / 2;
        }

        private void OnEnable()
        {
            inputActions.Enable();
        }

        private void OnDisable()
        {
            inputActions.Disable();
        }
    }
}
