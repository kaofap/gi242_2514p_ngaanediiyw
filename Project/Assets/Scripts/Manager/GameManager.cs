﻿using System;
using Assets.Scripts.Spaceship;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Assets.Scripts.Manager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private PlayerSpaceship playerSpaceship;
        [SerializeField] internal ScoreManager scoreManager;
        [SerializeField] private int playerSpaceshipHp;
        [SerializeField] private int playerSpaceshipMoveSpeed;
        [SerializeField] private TextMeshProUGUI HPPlayer;
        [SerializeField] public Image Fire01;
        [SerializeField] public Image Bomb;
        [SerializeField] public bool isLevel01;
        public event Action OnRestarted;
        [HideInInspector] public PlayerSpaceship spawnedPlayership;
        public static GameManager Instance { get; private set; }

        private void Awake()
        { 
            Debug.Assert(playerSpaceship != null,  "playerSpaceship cannot be null");
            Debug.Assert(scoreManager != null, "scoreManager cannot be null");
            Debug.Assert(playerSpaceshipHp > 0, "playerspaceship Hp has to be more than zero");
            Debug.Assert(playerSpaceshipMoveSpeed > 0, "playerSpaceshipMoveSpeed has to be more than zero");

            if (Instance == null)
            {
                Instance = this;
            }
        }

        private void Start()
        {
            StartGame();
        }
        
        private void Update()
        {
            Sethp(PlayerSpaceship.Instance.Hp);
        }

        private void StartGame()
        {
            scoreManager.Init(this);
            SpawnPlayerSpaceship();

        }

        private void SpawnPlayerSpaceship()
        {
            spawnedPlayership = Instantiate(playerSpaceship);
            spawnedPlayership.Init(playerSpaceshipHp, playerSpaceshipMoveSpeed);
            spawnedPlayership.OnExploded += OnPlayerSpaceshipExploded;
        }
        
        public void Sethp(int hp)
        {
            HPPlayer.text = $"HP Remain : {hp}";
        }

        private void OnPlayerSpaceshipExploded()
        {
            DestroyRemainingShips();
            if (isLevel01 == true)
            {
                SceneManager.LoadScene("Score01");
            }
            else
            {
                SceneManager.LoadScene("Score02");
            }
        }

        private void DestroyRemainingShips()
        {
            var enemyRemainingShips = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (var enemy in enemyRemainingShips)
            {
                Destroy(enemy);
            }
            
            var playerRemainingShips = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in playerRemainingShips)
            {
                Destroy(player);
            }
        }
    }
}
