﻿using TMPro;
using UnityEngine;

namespace Assets.Scripts.Manager
{
    public class ScoreManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;
        
        public static ScoreManager Instance { get; private set; }
        private GameManager gameManager;
        public int PlayerScore;
        public void Init(GameManager gameManager)
        {
            gameManager.OnRestarted += OnRestarted;
            HideScore(false);
            SetScore(0);
        }

        public void SetScore(int score)
        {
            PlayerScore += score;
            scoreText.text = $"Score : {PlayerScore}";
        }

        private void Awake()
        {
            PlayerScore = 0;
            Debug.Assert(scoreText != null, "scoreText cannot null");
            if (Instance == null)
            {
                Instance = this;
            }
        }

        private void OnRestarted()
        {
            gameManager.OnRestarted -= OnRestarted;
            HideScore(true);
            SetScore(0);
        }

        private void HideScore(bool hide)
        {
            scoreText.gameObject.SetActive(!hide);
        }
    }
}

