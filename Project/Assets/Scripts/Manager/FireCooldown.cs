﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.Manager
{
    public class FireCooldown : MonoBehaviour
    {
        [Header("Fire 1")] public Image Fire01;
        public float CoolDown = 5;
        private bool IsCooldown = false;

        public void Start()
        {
            Fire01.fillAmount = 0;
        }

        public void Fire1()
        {
        
        }
    
    }
}


