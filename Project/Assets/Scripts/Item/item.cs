﻿using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Manager;
using UnityEngine;

public class item : MonoBehaviour
{
    public void Bomball()
    {
        var enemyRemainingShips = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (var enemy in enemyRemainingShips)
        {
            Destroy(enemy);
            ScoreManager.Instance.SetScore(1);
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("Player"))
            {
                Bomball();
            }
        }
    }
}
